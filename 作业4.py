'''火车站都有大屏显示实时的火车信息，那么咱们也来模拟下显示大屏。
要求：使用序列存放车票信息，可以实时输入车票信息，并更新到序列中，然后实时更新。要求使用序列、循环、format（）格式化方法。
实时显示的信息如下：
车次      出发站-到达站    出发时间    到达时间     历时
T40       长春-北京        00:12       12:20       12:08
T298      长春-北京        00:06       10:50       10:44
Z158      长春-北京        12:48       21:06       08:18
Z62       长春-北京        21:58       06:08       08:20    '''

i = int(input("是否更新车票信息？（1表示是，0表示否）："))
if i == 1:
    x = 1
    list = []
    while x == 1:
        checi = str(input("请输入车次："))
        start = str(input("请输入出发站："))
        end = str(input("请输入到达站："))
        t1 = str(input("请输入出发时间："))
        t2 = str(input("请输入到达时间："))
        t = str(input("请输入经历时长："))              # 输入车票信息

        list.append([checi,start,end,t1,t2,t])        # 添加到list
        print("实时显示的信息如下：")
        print("车次\t出发站-到达站\t出发时间\t\t到达时间\t\t历时")
        temple = '{:s}\t\t{:s}-{:s}\t\t{:s}\t\t{:s}\t\t{:s}\t'
        for y in range(len(list)):
            print(temple.format(list[y][0],list[y][1],list[y][2],list[y][3],list[y][4],list[y][5]))

        answer = int(input("是否还需更新车票信息？（1表示是，0表示否）："))
        if answer == 1:
            x = 1
        else:
            x = 0
    print("车票信息更新完毕！")


