import socket
me = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
me.bind(('127.0.0.1', 8001))
me.listen()                                 # 监听
conn, address = me.accept()                 # 阻塞
data = conn.recv(1024)                      # 接收
print(data.decode())
conn.sendall(("服务器已接收到了内容："+str(data.decode())).encode())
me.close()