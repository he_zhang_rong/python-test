print("题目：王者荣耀游戏中有很多英雄角色，这些英雄也有类型上的区别，分别是坦克、战士、刺客、法师、射手、辅助。\n"
      "本次实践将应用Python中的列表存储不同类别的英雄，并遍历输出这些英雄。（编码中要体现增加、删除和修改一个元素的操作）\n")

坦克 = ["猪八戒","嫦娥","孙策","梦奇","苏烈","铠"]

战士 = ["马超","曜","云中君","盘古","李信","狂铁"]

刺客 = ["上官婉儿","司马懿","元歌","百里玄策","百里守约","孙悟空"]

法师 = ["西施","沈梦溪","米莱狄","弈星","杨玉环","女娲"]

射手 = ["蒙犽","伽罗","公孙离","黄忠","成吉思汗","虞姬"]

辅助 = ["鲁班大师","瑶","盾山","明世隐","鬼谷子","大乔"]

print("\n坦克类英雄：")
for item in 坦克:
    print(item + "\t\t",end='')
print("\n战士类英雄：")
for item in 战士:
    print(item + "\t\t",end='')
print("\n刺客类英雄：")
for item in 刺客:
    print(item + "\t\t",end='')
print("\n法师类英雄：")
for item in 法师:
    print(item + "\t\t",end='')
print("\n射手类英雄：")
for item in 射手:
    print(item + "\t\t",end='')
print("\n辅助类英雄：")
for item in 辅助:
    print(item + "\t\t",end='')

print("\n\n增加一个元素“王昭君”到法师中")
法师.insert(2,"王昭君")
print(法师)

print("\n删除法师中的一个元素“沈梦溪”")
del 法师[1]
print(法师)

print("\n将法师中的元素“杨玉环”修改成“妲己”")
法师[4] = "妲己"
print(法师)

'''
def printindex(a):
    for item in a:
        print(item)
printindex(战士)
printindex(坦克)
请忽略这部分(●'◡'●)
'''

print("-"*50)
print("排序bilibili2020年三月份番剧综合得分TOP10\n")
bili = [('异度侵入 ID:INVADED',262),('虚构推理',86),('某科学的超电磁炮T',31),('黑色四叶草',58),
        ('命运-冠位指定 绝对魔兽战线 巴比伦尼亚',156),('地缚少年花子君',34),('因为太怕痛就全点防御力了',87),
        ('Re：从零开始的异世界生活 新编集版',24),('入间同学入魔了',37),('魔法纪录 魔法少女小圆外传',22)]
print(bili)

print("\n增加一个元素")
bili.insert(0,('增加的元素',1))
print(bili)                                # 增加元素

print("\n删除一个元素")
del bili[0]
print(bili)                                # 删除元素

print("\n修改一个元素")
bili[-1] = ('博人传 火影忍者新时代',22)
print(bili)                                # 修改元素

print("\n排序后的TOP10番剧")
bili = sorted(bili,key=lambda bili:bili[1],reverse=True)
print(bili)                                # 排序元素