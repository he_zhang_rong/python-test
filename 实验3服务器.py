import socket
import base64
import os
from Cryptodome.Cipher import DES
import binascii

os.chdir(r"C:\Users\98606\Desktop\1924")
sy = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sy.bind(('127.0.0.1', 8001))
sy.listen()  # 监听
conn, address = sy.accept()  # 阻塞
print("来自", address, "的消息：", conn.recv(1024).decode())
conn.sendall("请发送密钥!".encode())
key = base64.b64decode(conn.recv(1024))
conn.sendall("已收到密钥!".encode())
print("密钥为：", key)
des = DES.new(key,DES.MODE_ECB)
print("来自", address, "的文件：", conn.recv(1024).decode())
data = conn.recv(1024)  # 接收
file = open("实验3.txt", "w")
file.write(des.decrypt(data).decode().rstrip(''))
file.close()
print("来自", address, "的信息：", des.decrypt(data).decode().rstrip(
    ' '), "已保存为实验3.txt")
conn.sendall("服务器已经收到了数据内容！".encode())
sy.close()