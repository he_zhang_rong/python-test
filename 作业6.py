print("定义一个类，课程、学生等，要求输出相关对象信息。\
（1）要求使用封装属性\
（2）要求使用继承属性\
（3）要求定义类的方法（不仅仅使用__init__()）\n")
class Lesson:
    _lesson = ''
    _name = ''
    _grade = ''
    def __init__(self):
        print("学生姓名")
    def Choice(self):
        print("该同学选择了：" + self._lesson)
        print("该同学成绩为：" + self._grade+"\n")

class XiaoMing(Lesson):
    _name = "小明"
    _lesson = "物理"
    _grade = "86"
    def __init__(self):
        print("学生姓名："+self._name)

class LiHong(Lesson):
    _name = "李红"
    _lesson = "数学"
    _grade = "98"
    def __init__(self):
        print("学生姓名："+self._name)

xiaoming = XiaoMing()
xiaoming.Choice()

lihong = LiHong()
lihong.Choice()