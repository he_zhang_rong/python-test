# 这是一个单行注释，下面是关于变量及其类型
x = 10
y = 10.0
z = "字符串"
print("x的类型是:",type(x))
print("y的类型是:",type(y))
print("z的类型是:",type(z))
print("输入a的值并空出一个缩进输出")
a = int(input("输入a的值："))
print("a的值为：\t",a)            # \t表示一个缩进
