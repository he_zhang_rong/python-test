print("使用SQLite建立数据库（自定义：人员、课程、游戏等数据库），建立一个表，并完成插入、删除、修改、查询的操作。")
import sqlite3
connect = sqlite3.connect("20192411.db")
cursor = connect.cursor()
# 建立一个表
cursor.execute('create table if not exists man (number int(10) primary key, name varchar(20), age int(10) ) ')
# 插入
cursor.execute('insert into man (number,name,age) values (01,"张一",31)')
cursor.execute('insert into man (number,name,age) values (02,"李二",33)')
cursor.execute('insert into man (number,name,age) values (03,"铁柱",26)')

# 查询
print("查询：")
cursor.execute('select * from man')
print(cursor.fetchall())

# 删除
print("删除操作：")
cursor.execute('delete from man where number = 03')
# 删除后的查询
cursor.execute('select * from man')
print(cursor.fetchall())

# 修改
print("修改操作：")
cursor.execute('update man set age = 36 where number = 02')
cursor.execute('select * from man')
print(cursor.fetchall())

# 关闭数据库
connect.close()