print("简易计算器（加减乘除，x在前，y在后）")
def jia(x,y):
    return x + y
def jian(x,y):
    return x - y
def cheng(x,y):
    return x * y
def chu(x,y):
    return x / y
while(1):
    t = str(input("是否进行计算？（1表示是，0表示否）："))
    if t == "1":
        a = int(input("请输入x的值："))
        b = int(input("请输入y的值："))
        c = str(input("要进行的运算（输入+，-，*，/）:"))

        if c == "+":
            print(a,"+",b,"=",jia(a,b))
        elif c == "-":
            print(a, "-", b, "=", jian(a, b))
        elif c == "*":
            print(a, "*", b, "=", cheng(a, b))
        elif c == "/":
            print(a, "/", b, "=", chu(a, b))
    elif t == "0":
        print("计算结束")
        break
    else:
        print("请输入1或0")