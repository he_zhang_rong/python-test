'''根据生日判断星座，要求输入出生的月份，调用判断星座的函数，返回具体的星座并输出（也可以把每个星座的性格特点也输出一下 ^_^）。
要求：用函数。
本题5分。根据实际情况酌情判分。超时提交作业扣分。
PS：
    n = ('摩羯座','水瓶座','双鱼座','白羊座','金牛座','双子座','巨蟹座','狮子座','处女座','天秤座','天蝎座','射手座','摩羯座')
    # 月份类别
    d = (20,19,21,20,21,22,23,23,23,24,23,22)

水瓶座：1月21日 - 2月19日
双鱼座：2月20日 - 3月20日
白羊座：3月21日 - 4月20日
金牛座：4月21日 - 5月21日
双子座：5月22日 - 6月21日
巨蟹座：6月22日 - 7月22日
狮子座：7月23日 - 8月23日
处女座：8月24日 - 9月23日
天秤座：9月24日 - 10月23日
天蝎座：10月24日 - 11月22日
射手座：11月23日 - 12月21日
摩羯座：12月22日 - 1月20日'''

print("根据生日判断星座，要求输入出生的月份，调用判断星座的函数")
def xingzuo(month,date):
    '''功能：根据生日判断星座，要求输入出生的月份，调用判断星座的函数
       返回值：具体的星座
    '''
    xz = ('摩羯座','水瓶座','双鱼座','白羊座','金牛座','双子座','巨蟹座','狮子座','处女座','天秤座','天蝎座','射手座','摩羯座')
    d = (20,19,21,20,21,22,23,23,23,24,23,22)
    if date < d[month-1]:
        return xz[month-1]
    else:
        return xz[month]
# 调用函数
month = int(input("请输入月份:"))
date = int(input("请输入日期:"))
print(xingzuo(month,date))