print("最近疫情稳定了不少，好久没出去打篮球了，今天天气怎么样呢？\n")
tem = eval(input("请输入今天的温度（单位：℃）："))
if tem < 15:
    print("今天温度太低咯！打球容易着凉~")                         # 低温时
elif tem >=15 and tem <30:
    print("今天温度正合适，湿度怎么样呢？")                        # 温度适中时
    hum = eval(input("请输入今天的湿度(单位：%)："))
    if hum >= 50 and hum <= 60:
        print("今天的湿度最让人感到舒适，那是否会刮风呢？")
        wind = str(input("今天是否刮风（1表示是，0表示否）"))
        if wind == "1":
            print("可惜了，刮风打球不爽。")
        else:
            print("Nice！今天天气正适合打球！赶快出发吧~")
    else:
        print("今天湿度不合适打球呢！")
else:
    print("今天太热啦，打球容易中暑。")
