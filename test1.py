class Item:
    _name = ''
    _useFor = ''

    def __init__(self):
        print("物品类")

    def _use(self):
        print("我可以" + self._useFor + "\n")

class ColdDrink(Item):
    _name = "冷饮"
    _useFor = "解除酷暑状态"

    def __init__(self):
        print("我是" + self._name)


ColdDrink1 = ColdDrink()
ColdDrink1._use()

